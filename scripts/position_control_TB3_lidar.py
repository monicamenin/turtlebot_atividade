#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from sensor_msgs.msg import LaserScan
import numpy as np
import time

class TurtleControl:
    def __init__(self):
        rospy.init_node("tb3control_node", anonymous=True)
        self.vel_publisher = rospy.Publisher("tb3_1/cmd_vel", Twist, queue_size=10)
        self.vel_publisher_master = rospy.Publisher("tb3_0/cmd_vel", Twist, queue_size=10)
        rospy.Subscriber("tb3_1/scan", LaserScan, self.update_scan)
        rospy.Subscriber("tb3_1/odom", Odometry, self.update_pose)
        self.rate = rospy.Rate(10)
        self.pose = Pose()
        self.ref_pose = Pose()
        self.max_vel = 0.22
        self.max_ang = 2.84
        self.scan = LaserScan()
        self.scanant = LaserScan()        

    def update_pose(self, msg):
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
        (_, _, yaw) = euler_from_quaternion(orientation_list)
        self.pose.x = msg.pose.pose.position.x
        self.pose.y = msg.pose.pose.position.y
        self.pose.theta =  yaw

    def update_scan(self, msg):
    	self.scan = msg

    def ref_distance(self):
        return np.sqrt(  (self.ref_pose.x - self.pose.x)**2 + (self.ref_pose.y - self.pose.y)**2)

    def linear_vel_control(self, kp = 1.5):
        distance = self.ref_distance()
        control = kp* distance
        if abs(control) > self.max_vel:
            control = self.max_vel*np.sign(control)
        return control

    def angular_vel_control(self, kp=6):
        angle_r = np.arctan2(self.ref_pose.y - self.pose.y,  self.ref_pose.x - self.pose.x )        
        control = kp*(angle_r - self.pose.theta)
        if abs(control) > self.max_ang:
            control = self.max_ang*np.sign(control)
        return control

    def detecta_mov(self):
        detectarobj = np.zeros(360)
        for i in range(360):
            if self.scan.ranges[i] <= 3.5 and self.scanant.ranges[i] <= 3.5:
                detectarobj[i] = self.scan.ranges[i] - self.scanant.ranges[i]
            else:
                detectarobj[i] = 0
        angref = detectarobj.argmax()
        distref = self.scan.ranges[angref]
        if detectarobj[angref] >= 0.001:
            self.ref_pose.x = self.pose.x + (distref * np.cos(self.pose.theta + np.deg2rad(angref)))
            self.ref_pose.y = self.pose.y + (distref * np.sin(self.pose.theta + np.deg2rad(angref)))
        else:
            self.ref_pose.x = self.pose.x
            self.ref_pose.y = self.pose.y

    def move2ref(self):
        ref_tol = 0.2
        vel_msg = Twist()
        while not rospy.is_shutdown():
            self.scanant = self.scan
            time.sleep(1)
            self.detecta_mov()
            while self.ref_distance() >= ref_tol:
                vel_msg.linear.x = 0
                vel_msg.linear.y = 0
                vel_msg.linear.z = 0
                vel_msg.angular.x = 0
                vel_msg.angular.y = 0
                vel_msg.angular.z = 0
                #laço para detectar objetos e desviar
                while self.scan.ranges[15] < 0.5 or self.scan.ranges[0] < 0.5 or self.scan.ranges[345] < 0.5:
                    rospy.loginfo("Objeto identificado")
                    if self.scan.ranges[0] < 0.5: #obstaculo na frente
                        vel_msg.linear.x = -1
                        vel_msg.angular.z = 0
                        self.vel_publisher.publish(vel_msg)
                    elif self.scan.ranges[15] < 0.5: #obstaculo na direita
                        vel_msg.linear.x = 0.2
                        vel_msg.angular.z = -1
                        self.vel_publisher.publish(vel_msg)
                    else: #obstaculo na esquerda
                        vel_msg.linear.x = 0.2
                        vel_msg.angular.z = 1
                        self.vel_publisher.publish(vel_msg)
                        
                vel_msg.linear.x = self.linear_vel_control()
                vel_msg.angular.z = self.angular_vel_control()
                self.vel_publisher.publish(vel_msg)
                self.rate.sleep()

            # stop
            vel_msg.linear.x = 0
            vel_msg.angular.z= 0
            self.vel_publisher.publish(vel_msg)
            

if __name__ == '__main__':
    bot = TurtleControl()
    time.sleep(5)
    bot.move2ref()
